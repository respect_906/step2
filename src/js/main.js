document.getElementById("nav-menu-icon").addEventListener("click", openMenu);
function openMenu() {
document.getElementById("nav-submenu").classList.toggle("active")
}

// scroll--------------------------------------------\

$(document).ready(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > $(window).height()) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
// -------------------